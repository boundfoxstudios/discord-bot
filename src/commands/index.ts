import { ImageUploadCommand } from './image-upload.command';
import { InfoCommand } from './info.command';

export const Commands = [InfoCommand, ImageUploadCommand];
