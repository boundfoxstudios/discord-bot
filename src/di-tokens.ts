export const DiTokens = {
  Configuration: Symbol.for('Configuration'),
  Command: Symbol.for('Command'),
  DiscordClient: Symbol.for('DiscordClient'),
  CommandProvider: Symbol.for('CommandProvider'),
};
