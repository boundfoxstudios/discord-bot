# MOVED

The code has been moved to: https://github.com/boundfoxstudios/discord-bot


# Boundfox Studios Discord Bot

This is the official Boundfox Studios Discord Bot.

It's not a general purpose bot, but tied to the stuff we need at Boundfox Studios.

However, we decided to make it Open Source, if you want to take a look how things are working. :-)
